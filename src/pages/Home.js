import React, { useState } from 'react'
import { View,Text,FlatList, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, Animated } from 'react-native'
import {Picker} from '@react-native-picker/picker';
import _ from "lodash"
import { AntDesign } from '@expo/vector-icons'; 
import Header from '../components/Header'
import data from '../assets/mock.json'
import Cell from '../components/Cell'

function Home() {
    const scrollY = new Animated.Value(0)
    const scrollX = new Animated.Value(0)


    const diffClamp = Animated.diffClamp(scrollY,0,50)
    const translateY = diffClamp.interpolate({
        inputRange:[0,50],
        outputRange:[0,-50]
    })

    const translateX = scrollX.interpolate({
        inputRange:[0,400],
        outputRange:[0,400]
    })

    const [myData,setMyData] = useState(data)
    const columns  = Object.keys(data[0])
    const [ direction, setDirection ] = useState(null)
    const [ selectedColumn, setSelectedColumn ] = useState(null)
    const  [teamfilter,setTeamFilter]=useState('');
    const [targetFilter,setTargetFilter]= useState(null)

    const fetchTeams = ()=> {
        const teams = new Set()
        for(const row of data){
            teams.add(row["Team Name"])
        }
        return Array.from(teams)
    }
    const filterData = () =>{
        var filteredData = data

        if(targetFilter != null ){
            filteredData = filteredData.filter((row)=>{return row.Target==targetFilter})
        }

        if(teamfilter != ''){
            filteredData=filteredData.filter((row)=>{
                return row["Team Name"] === teamfilter
            })
                 }

        // for(teamfilter of teamfilters){
        //     filterData.filter((row)=>{ return row["Team Name"] == teamfilter})
        // }
        setMyData(filteredData)
       
    }

    const sortTable = (column) => {
        const newDirection = direction === "desc" ? "asc" : "desc" 
        const sortedData = _.orderBy(myData, [column],[newDirection])
        setSelectedColumn(column)
        setDirection(newDirection)
        setMyData(sortedData)
    }
    
    return ( 

        <SafeAreaView style={{flex:1,paddingTop:50}}>
            <View style={{flexDirection:"row",justifyContent:"space-between",height:50,padding:10}}>
                <Text>Filters</Text>
                <View style={{flexDirection:"row"}}>
                    <Picker
                        style={{width:150}}
                        selectedValue={teamfilter}
                        onValueChange={(itemValue, itemIndex) =>{
                            setTeamFilter(itemValue)
                            filterData()
                        }}>
                                <Picker.Item  key='0' label="Team" value='' />
                        {
                            fetchTeams().map((team)=>{
                                return <Picker.Item  key={team} label={team} value={team} />
                            })
                        }   
                    </Picker>
                    <Picker
                        style={{width:150}}
                            selectedValue={targetFilter}
                            onValueChange={(itemValue, itemIndex) =>{
                                setTargetFilter(itemValue)
                                filterData()
                            }}>
                            <Picker.Item  key='0' label="Target" value={null} />
                            <Picker.Item key="yes" label="Yes" value={true} />
                            <Picker.Item key = "no" label="No" value={false} />
                    </Picker>
                </View>
            </View>

            <Animated.ScrollView 
                onScroll={
                // (e)=>{
                //     scrollX.setValue(e.nativeEvent.contentOffset.x)
                // }
                Animated.event([{
                    nativeEvent:{
                        contentOffset:{
                            x:scrollX
                        }
                    }
                }],{useNativeDriver:true})
            }
                contentContainerStyle={{width:600,flexDirection:"column"}}  horizontal
            >    
                <Animated.View style={{transform:[{translateY:translateY}],elevation:4,zIndex:0}}>
                    <Header columns={columns} direction={direction} sortTable={sortTable} selectedColumn={selectedColumn} />
                </Animated.View>
                    <View style={{flexDirection:"row"}}>
                        <FlatList
                            contentContainerStyle={{paddingTop:50}}
                            data={myData}
                            renderItem={({item, index})=> {
                                const color = index % 2 == 1 ? "#F0FBFC" : "white"
                                return (
                                    
                                    <View style={{...styles.tableRow, backgroundColor: color}}>
                                        {
                                            columns.map((column,index)=>{
                                                return(
                                                (index<=1)
                                                ? 
                                                <Animated.View style={[styles.columnRowTxt,{backgroundColor:color,zIndex:1,transform:[{translateX:translateX}]}]}>
                                                    {(index==0)
                                                    ?
                                                        <Text style={[styles.columnRowTxt,(index==0)?styles.sticky:{}]}> {item[column]} </Text>    
                                                    :
                                                        (item["Target"]==true)
                                                        ?
                                                            <AntDesign name="checkcircle" size={16} color="green" />
                                                        :
                                                            <AntDesign name="minuscircle" size={16} color="grey" />
                                                    }
                                                </Animated.View>
                                                
                                                : 
                                                <Text style={[styles.columnRowTxt,(index==0)?styles.sticky:{}]}> {item[column]} </Text>    
                                                ) 
                                                
                                            })
                                        }
                                    </View>
                                )   
                            }}
                            onScroll={(e)=>{
                               scrollY.setValue(e.nativeEvent.contentOffset.y)
                            }}
                        />
                    </View>
            </Animated.ScrollView>
           
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop:80
    },
    tableRow: {
        flexDirection: "row",
        height: 40,
        alignItems:"center",
        backgroundColor:"#fff"
    },
    columnRowTxt: {
        width:"20%",
        maxWidth:100,
        textAlign:"center",
        alignItems:"center",
    },
})  

export default Home
