import React from 'react'
import{View,Text,TouchableOpacity, StyleSheet} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons';


function Header({columns,direction,selectedColumn,sortTable}) {
    return (
        <View style={styles.tableHeader}>
        {
          columns.map((column, index) => {
            {
              return (
                <TouchableOpacity 
                  key={index}
                  style={styles.columnHeader} 
                  onPress={()=> sortTable(column)}>
                  <Text style={styles.columnHeaderTxt}>{column + " "} 
                    { selectedColumn === column && <MaterialCommunityIcons 
                        name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
                      />
                    }
                  </Text>
                </TouchableOpacity>
              )
            }
          })
        }
      </View>
    )
}

const styles = StyleSheet.create({
    tableHeader: {
        position:"absolute",
        top:0,
        left:0,
        right:0,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        backgroundColor: "#37C2D0",
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        height: 50
      },
      columnHeader: {
        width: "20%",
        maxWidth:100,
        justifyContent: "center",
        alignItems:"center"
      },
      columnHeaderTxt: {
        color: "white",
        fontWeight: "bold",
      },
})
export default Header;