import React from 'react'
import { StyleSheet, Text,View } from 'react-native'

function Cell({text,header=false}) {
    return (
        <View style={style.cellContainer,(header)?style.headerCell:{}}>
            <Text>{text}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    cellContainer:{
        borderWidth:1,
        borderColor:"#000",
        minWidth:50,
        maxWidth:100,
        padding:5
    },
    headerCell:{
        backgroundColor:"#eee"
    }
})
export default Cell
